package net.pl3x.bukkit.pl3xitemcommand.configuration;

import net.pl3x.bukkit.pl3xitemcommand.ItemCommand;
import net.pl3x.bukkit.pl3xitemcommand.ItemUtil;
import net.pl3x.bukkit.pl3xitemcommand.Logger;
import net.pl3x.bukkit.pl3xitemcommand.Pl3xItemCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";

    public static void reload() {
        Pl3xItemCommand plugin = Pl3xItemCommand.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");

        ConfigurationSection itemCommands = config.getConfigurationSection("item-commands");
        if (itemCommands == null) {
            return;
        }

        plugin.getItemCommandManager().clearAll();
        for (String sectionName : itemCommands.getKeys(false)) {
            ConfigurationSection section = itemCommands.getConfigurationSection(sectionName);
            if (section == null) {
                Logger.warn("Corrupt yaml section: item-commands." + sectionName);
                continue;
            }

            ItemStack item = ItemUtil.getItemStack(section.getConfigurationSection("item"));
            if (item == null) {
                Logger.warn("Corrupt item config: item-commands." + sectionName + ".item");
                continue;
            }

            boolean consumeOnUse = section.getBoolean("consume-on-use", false);
            List<String> commands = section.getStringList("commands");
            String notice = section.getString("notice");

            plugin.getItemCommandManager().addItemCommand(new ItemCommand(item, commands, notice, consumeOnUse));
        }
    }
}
