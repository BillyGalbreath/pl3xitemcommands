package net.pl3x.bukkit.pl3xitemcommand;

import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;

public class ItemCommandManager {
    private Set<ItemCommand> itemCommands = new HashSet<>();

    public ItemCommand getItemCommand(ItemStack item) {
        for (ItemCommand itemCommand : itemCommands) {
            if (ItemUtil.equals(itemCommand.getItem(), item)) {
                return itemCommand;
            }
        }
        return null;
    }

    public boolean addItemCommand(ItemCommand itemCommand) {
        return getItemCommand(itemCommand.getItem()) == null && itemCommands.add(itemCommand);
    }

    public void clearAll() {
        itemCommands.clear();
    }
}
