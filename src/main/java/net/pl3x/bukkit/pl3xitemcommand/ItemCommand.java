package net.pl3x.bukkit.pl3xitemcommand;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ItemCommand {
    private final ItemStack item;
    private final List<String> commands;
    private final String notice;
    private final boolean consumeOnUse;

    public ItemCommand(ItemStack item, List<String> commands, String notice, boolean consumeOnUse) {
        this.item = item;
        this.commands = commands;
        this.notice = notice;
        this.consumeOnUse = consumeOnUse;
    }

    public ItemStack getItem() {
        return item;
    }

    public List<String> getCommands() {
        return commands;
    }

    public String getNotice() {
        return notice;
    }

    public boolean consumeOnUse() {
        return consumeOnUse;
    }
}
