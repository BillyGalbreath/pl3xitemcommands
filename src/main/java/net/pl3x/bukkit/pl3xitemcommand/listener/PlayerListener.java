package net.pl3x.bukkit.pl3xitemcommand.listener;

import net.pl3x.bukkit.pl3xitemcommand.ItemCommand;
import net.pl3x.bukkit.pl3xitemcommand.ItemUtil;
import net.pl3x.bukkit.pl3xitemcommand.Pl3xItemCommand;
import net.pl3x.bukkit.pl3xitemcommand.api.ItemCommandEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {
    private final Pl3xItemCommand plugin;

    public PlayerListener(Pl3xItemCommand plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onClickCommandItem(PlayerInteractEvent event) {
        if (event.getHand() != EquipmentSlot.HAND) {
            return; // only listen to main hand
        }

        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return; // did not right click
        }

        if (runCommands(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClickCommandItem(PlayerInteractEntityEvent event) {
        if (event.getHand() != EquipmentSlot.HAND) {
            return; // only listen to main hand
        }

        if (event.getRightClicked() == null) {
            return; // did not right click
        }

        if (runCommands(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    private boolean runCommands(Player player) {
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            return false; // not holding anything in hand
        }

        ItemCommand itemCommand = plugin.getItemCommandManager().getItemCommand(item);
        if (itemCommand == null) {
            return false; // no command assigned to item
        }

        ItemCommandEvent event = new ItemCommandEvent(player, itemCommand);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return false; // another plugin cancelled the event
        }

        if (itemCommand.consumeOnUse() && !ItemUtil.takeItem(player, item)) {
            return false; // not holding the initial item anymore
        }

        for (String command : itemCommand.getCommands()) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command
                    .replace("{player}", player.getName()));
        }

        String notice = itemCommand.getNotice();
        if (notice != null && !notice.isEmpty()) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', notice));
        }
        return true;
    }
}
