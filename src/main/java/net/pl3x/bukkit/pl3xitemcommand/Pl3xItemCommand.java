package net.pl3x.bukkit.pl3xitemcommand;

import net.pl3x.bukkit.pl3xitemcommand.api.ItemNBT;
import net.pl3x.bukkit.pl3xitemcommand.command.CmdPl3xItemCommand;
import net.pl3x.bukkit.pl3xitemcommand.configuration.Config;
import net.pl3x.bukkit.pl3xitemcommand.configuration.Lang;
import net.pl3x.bukkit.pl3xitemcommand.listener.PlayerListener;
import net.pl3x.bukkit.pl3xitemcommand.nms.ItemNBTHandler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xItemCommand extends JavaPlugin {
    private ItemNBT nbtHandler;
    private ItemCommandManager itemCommandManager;

    public Pl3xItemCommand() {
        nbtHandler = new ItemNBTHandler();
        itemCommandManager = new ItemCommandManager();
    }

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

        getCommand("pl3xitemcommand").setExecutor(new CmdPl3xItemCommand(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static Pl3xItemCommand getPlugin() {
        return Pl3xItemCommand.getPlugin(Pl3xItemCommand.class);
    }

    public ItemNBT getNBTHandler() {
        return nbtHandler;
    }

    public ItemCommandManager getItemCommandManager() {
        return itemCommandManager;
    }
}
