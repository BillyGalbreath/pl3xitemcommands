package net.pl3x.bukkit.pl3xitemcommand.api;

import org.bukkit.inventory.ItemStack;

public interface ItemNBT {
    ItemStack setItemNBT(ItemStack bukkitItem, String nbt, String path);
}
