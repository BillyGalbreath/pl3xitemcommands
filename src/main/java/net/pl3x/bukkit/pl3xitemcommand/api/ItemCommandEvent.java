package net.pl3x.bukkit.pl3xitemcommand.api;

import net.pl3x.bukkit.pl3xitemcommand.ItemCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ItemCommandEvent extends PlayerEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private final ItemCommand itemCommand;

    private boolean cancelled = false;

    public ItemCommandEvent(Player player, ItemCommand itemCommand) {
        super(player);
        this.itemCommand = itemCommand;
    }

    public ItemCommand getItemCommand() {
        return itemCommand;
    }

    public ItemStack getItem() {
        return itemCommand.getItem();
    }

    public List<String> getCommands() {
        return itemCommand.getCommands();
    }

    public String getNotice() {
        return itemCommand.getNotice();
    }

    public boolean willComsume() {
        return itemCommand.consumeOnUse();
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
